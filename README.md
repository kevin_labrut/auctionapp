# AuctionApp
## Introduction

**AuctionAppr** is server-side solution of the project TEST TEADS.

NO Architectural Board :)

## Pre-requirements

You should have Node.js install on your system (https://nodejs.org/en/).

You should install newman globally if you want to test the api :

```sh
npm install -g newman 
```

## Tests


### API Test

First of all you must build the project with :
```sh
npm run build
```

You have to launch the server with :
```sh
npm run start
```
In an another terminal launch this command :
```sh
newman run test/auctionTest.postman_collection.postman_collection.json
```

### Loadtest

First of all you must build the project with :
```sh
npm run build
```

You have to launch the server with :
```sh
npm run start
```
In an another terminal launch this command :
```sh
newman run test/auctionTest.postman_collection.postman_collection.json --iteration-count 1000
```

## Authors
Kevin LABRUT (kevin.labrut@gmail.com) - version: 1.0.0
