FROM node:10.15.3-alpine
ENV PORT=3000

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install

COPY src ./src
COPY ts*.json ./

RUN npm run build

EXPOSE ${PORT}

CMD ["npm", "start"]