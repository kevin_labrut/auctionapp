import express, { NextFunction, Request, Response } from "express";
import { Auction, BuyerWinner } from "../models";
import HttpException from "../exceptions/http-exceptions";
import { HTTPCode } from "../http-code";
const router = express.Router();

interface getBuyerResponse{
    name?: string;
    price?: number;
    message: string;
}

/* First method to determine the buyer */
router.post("/getBuyerAndPriceFirstMethod", (request: Request, response: Response, nextFunction: NextFunction) => {
    // validate request
    if (validateAuction(request)) {
        const action: Auction = { ...request.body };
        const result = getBuyerAndPriceFirstMethod(action);
        response.status(HTTPCode.Success).send(result);
    } else {
        nextFunction(new HttpException(HTTPCode.BadRequest, "Bad request :'("))
    }
});

/* Second method to determine the buyer */
router.post("/getBuyerAndPriceSecondMethod", (request: Request, response: Response, nextFunction: NextFunction) => {
    // validate request
    if (validateAuction(request)) {
        const auction: Auction = { ...request.body };
        const result = getBuyerAndPriceSecondMethod(auction);
        response.status(HTTPCode.Success).send(result);
    } else {
        nextFunction(new HttpException(HTTPCode.BadRequest, "Bad request :'("))
    }
});

function getBuyerAndPriceFirstMethod(auction: Auction) {
    // get the max price by buyer, remove buyer poor :) and store first and second best buyer
    const maxPriceByBuyer =
        auction.buyers
            .map((buyer) => ({ buyerName: buyer.name, maxPrice: Math.max(...buyer.bids) })) // spread power
            .filter((buyer) => buyer.maxPrice >= auction.objectPrice)
            .sort((buyer1, buyer2) => buyer2.maxPrice - buyer1.maxPrice)
            .slice(0, 2);


     let result: getBuyerResponse;
        
        if(maxPriceByBuyer[0]){
            result = {
                name: maxPriceByBuyer[0].buyerName,
                price: maxPriceByBuyer[1] ? maxPriceByBuyer[1].maxPrice : auction.objectPrice,
                message: `The buyer ${maxPriceByBuyer[0].buyerName} wins the auction at the price of ${maxPriceByBuyer[1] ? maxPriceByBuyer[1].maxPrice : auction.objectPrice} euros.`
            }
        }else{
            result = {
                message: `No buyer :'(`
            }
        }
    return result;

}

function getBuyerAndPriceSecondMethod(auction: Auction) {

    const buyerWinner = auction.buyers.reduce<BuyerWinner>((buyerWinner, buyer) => {
        const maxPrice: number = Math.max(...buyer.bids);
        if (maxPrice > buyerWinner.maxPriceBuyer) {
            buyerWinner.buyer = buyer;
            buyerWinner.maxPriceBuyer = maxPrice;
        } else if (buyerWinner.price < maxPrice) {
            buyerWinner.price = maxPrice;
        }
        return buyerWinner;
    }, new BuyerWinner(null, auction.objectPrice));

    let result: getBuyerResponse;
    //if a winner exist and its price is over the objectPrice
    if(buyerWinner.buyer && buyerWinner.maxPriceBuyer >= auction.objectPrice){
        result = {
            name: buyerWinner.buyer.name,
            price: buyerWinner.price > auction.objectPrice ? buyerWinner.price : auction.objectPrice,
            message: `The buyer ${buyerWinner.buyer ? buyerWinner.buyer.name: ''} wins the auction at the price of ${buyerWinner.price} euros.`
        }
    }else{
        result = {
            message: `No buyer :'(`
        }
    }

    return result;
}

function validateAuction(request: Request) {
    if (!request.body) {
        return false;
    }

    // validate name auction, string required
    if (!request.body.name || typeof request.body.name !== "string") {
        return false;
    }

    // validate objectPrice auction, number required
    if (!request.body.objectPrice || typeof request.body.objectPrice !== "number") {
        return false;
    }

    // validate objectPrice auction, object (array) required
    if (!request.body.buyers || typeof request.body.buyers !== "object") {
        return false;
    }

    return true;
}

export default router;