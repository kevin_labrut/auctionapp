import { Buyer } from "./buyer";

export class Auction {
    constructor(
        public name: string,
        public objectPrice: number,
        public buyers: Buyer[]) { }
}
