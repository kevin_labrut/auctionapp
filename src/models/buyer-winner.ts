import { Buyer } from "./buyer";

export class BuyerWinner {
    constructor(
        public buyer: Buyer | null = null,
        public price: number = 0,
        public maxPriceBuyer: number = 0,
    ) { }
}
