export class Buyer {
    constructor(
        public name: string,
        public bids: number[]) { }
}
