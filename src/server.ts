import express from "express";
import helmet from "helmet";
import morgan from "morgan";
import bodyParser from "body-parser";

import { Request, NextFunction, Response } from "express";
import { HTTPCode } from "./http-code";
import HttpException from "./exceptions/http-exceptions";

import auctionRouter from "./routes/auction-route";

// get env vars, default 3000.
const port = +(process.env.PORT || 3000);

/**
 * Express settings
 */
const app: express.Application = express();

app.use(morgan('tiny')); // log API request
app.use(helmet()); // secure context
app.use(bodyParser.json({ limit: '1Mb' })); // fix max body payload size, auction must be can't over 1 mb :)

// ping route
app.get("/ping", (req, res) => {
    res.status(200).send("PONG");
});

// auction route
app.use("/auction", auctionRouter);

// route not found, catch BadRequest and forward to error handler
app.use((request: Request, response: Response, nextFunction: NextFunction) => {
    nextFunction(new HttpException(HTTPCode.BadRequest));
});

// error handling
app.use((error: HttpException, request: Request, response: Response, nextFunction: NextFunction) => {
    const status = error.status || HTTPCode.InternalServerError;
    const message = error.message || 'Something went wrong';
    response
        .status(status)
        .send({
            status,
            message,
        })
});

app.listen(port, () => { console.log(`Server is listening on port ${port}`); });


