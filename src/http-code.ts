export enum HTTPCode {
    Success = 200,
    BadRequest = 400,
    Unauthenticated = 401,
    InternalServerError = 500,
}
